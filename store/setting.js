export const state = () => ({
  //openModalSetting
  open_setting: 'false',

  // user setting from db
  user_setting: {},

  current_route: '/chapters'
})

export const mutations = {
  // update whole user setting
  UPDATE_user_setting(state, payload) {
    state.user_setting = payload
  },
  // user setting specific
  UPDATE_theme(state, payload) {
    state.user_setting.theme = payload
  },
  UPDATE_verse_translation_language(state, payload) {
    state.user_setting.verse_translation_language = payload
  },
  UPDATE_word_translation_language(state, payload) {
    state.user_setting.word_translation_language = payload
  },
  UPDATE_word_by_word_mode(state, payload) {
    state.user_setting.word_by_word_mode = payload
  },

  UPDATE_arabic_size(state, payload) {
    state.user_setting.arabic_size = payload
  },
  UPDATE_word_translation_size(state, payload) {
    state.user_setting.word_translation_size = payload
  },
  UPDATE_verse_translation_size(state, payload) {
    state.user_setting.verse_translation_size = payload
  },

  //modal
  UPDATE_open_setting(state, payload) {
    state.open_setting = payload
  },

  //current route

  UPDATE_current_route(state, payload) {
    state.current_route = payload
  }
}

export const actions = {
  async FETCH(context) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(`/auth/setting`)
    context.commit('UPDATE_user_setting', res)
    return res
  },

  async STORE(context) {
    const data = {
      theme: context.state.user_setting.theme,
      verse_translation_language:
        context.state.user_setting.verse_translation_language,
      word_translation_language:
        context.state.user_setting.word_translation_language,
      word_by_word_mode: context.state.user_setting.word_by_word_mode,

      arabic_size: context.state.user_setting.arabic_size,
      word_translation_size: context.state.user_setting.word_translation_size,
      verse_translation_size: context.state.user_setting.verse_translation_size
    }

    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$post(`/auth/setting`, data)
    context.commit('UPDATE_user_setting', res)
    return res
  }
}
