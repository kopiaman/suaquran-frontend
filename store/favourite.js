export const state = () => ({
  favourites: []
})

export const mutations = {
  UPDATE_favourites(state, payload) {
    state.favourites = payload
  }
}

export const actions = {
  async INDEX(context, id) {
    const params = {
      language_name: context.rootState.setting.language_name
    }

    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(`/favourites`, { params })
    context.commit('UPDATE_favourites', res)
    return res
  },

  async STORE(context, id) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$post(`/favourites/${id}`)
    return res
  },

  async REMOVE(context, id) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$delete(`/favourites/${id}/remove`)
    return res
  }
}
