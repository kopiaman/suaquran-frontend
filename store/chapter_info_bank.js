export const state = () => ({
  // 0x
  chapter_1: [],
  chapter_2: [],
  chapter_3: [],
  chapter_4: [],
  chapter_5: [],
  chapter_6: [],
  chapter_7: [],
  chapter_8: [],
  chapter_9: [],

  //1x
  chapter_10: [],
  chapter_11: [],
  chapter_12: [],
  chapter_13: [],
  chapter_14: [],
  chapter_15: [],
  chapter_16: [],
  chapter_17: [],
  chapter_18: [],
  chapter_19: [],

  //2x
  chapter_20: [],
  chapter_21: [],
  chapter_22: [],
  chapter_23: [],
  chapter_24: [],
  chapter_25: [],
  chapter_26: [],
  chapter_27: [],
  chapter_28: [],
  chapter_29: [],

  //3x
  chapter_30: [],
  chapter_31: [],
  chapter_32: [],
  chapter_33: [],
  chapter_34: [],
  chapter_35: [],
  chapter_36: [],
  chapter_37: [],
  chapter_38: [],
  chapter_39: [],

  //4x
  chapter_40: [],
  chapter_41: [],
  chapter_42: [],
  chapter_43: [],
  chapter_44: [],
  chapter_45: [],
  chapter_46: [],
  chapter_47: [],
  chapter_48: [],
  chapter_49: [],

  //5x
  chapter_50: [],
  chapter_51: [],
  chapter_52: [],
  chapter_53: [],
  chapter_54: [],
  chapter_55: [],
  chapter_56: [],
  chapter_57: [],
  chapter_58: [],
  chapter_59: [],

  // 6x
  chapter_60: [],
  chapter_61: [],
  chapter_62: [],
  chapter_63: [],
  chapter_64: [],
  chapter_65: [],
  chapter_66: [],
  chapter_67: [],
  chapter_68: [],
  chapter_69: [],

  //7x
  chapter_70: [],
  chapter_71: [],
  chapter_72: [],
  chapter_73: [],
  chapter_74: [],
  chapter_75: [],
  chapter_76: [],
  chapter_77: [],
  chapter_78: [],
  chapter_79: [],

  // 8x
  chapter_80: [],
  chapter_81: [],
  chapter_82: [],
  chapter_83: [],
  chapter_84: [],
  chapter_85: [],
  chapter_86: [],
  chapter_87: [],
  chapter_88: [],
  chapter_89: [],

  // 9x
  chapter_90: [],
  chapter_91: [],
  chapter_92: [],
  chapter_93: [],
  chapter_94: [],
  chapter_95: [],
  chapter_96: [],
  chapter_97: [],
  chapter_98: [],
  chapter_99: [],

  // 100x
  chapter_100: [],
  chapter_101: [],
  chapter_102: [],
  chapter_103: [],
  chapter_104: [],
  chapter_105: [],
  chapter_106: [],
  chapter_107: [],
  chapter_108: [],
  chapter_109: [],

  // 11x
  chapter_110: [],
  chapter_111: [],
  chapter_112: [],
  chapter_113: [],
  chapter_114: []
})

export const mutations = {
  // 0x
  UPDATE_chapter_1(state, payload) {
    state.chapter_1 = payload
  },
  UPDATE_chapter_2(state, payload) {
    state.chapter_2 = payload
  },
  UPDATE_chapter_3(state, payload) {
    state.chapter_3 = payload
  },
  UPDATE_chapter_4(state, payload) {
    state.chapter_4 = payload
  },
  UPDATE_chapter_5(state, payload) {
    state.chapter_5 = payload
  },
  UPDATE_chapter_6(state, payload) {
    state.chapter_6 = payload
  },
  UPDATE_chapter_7(state, payload) {
    state.chapter_7 = payload
  },
  UPDATE_chapter_8(state, payload) {
    state.chapter_8 = payload
  },
  UPDATE_chapter_9(state, payload) {
    state.chapter_9 = payload
  },

  //   1x
  UPDATE_chapter_10(state, payload) {
    state.chapter_10 = payload
  },
  UPDATE_chapter_11(state, payload) {
    state.chapter_11 = payload
  },
  UPDATE_chapter_12(state, payload) {
    state.chapter_12 = payload
  },
  UPDATE_chapter_13(state, payload) {
    state.chapter_13 = payload
  },
  UPDATE_chapter_14(state, payload) {
    state.chapter_14 = payload
  },
  UPDATE_chapter_15(state, payload) {
    state.chapter_15 = payload
  },
  UPDATE_chapter_16(state, payload) {
    state.chapter_16 = payload
  },
  UPDATE_chapter_17(state, payload) {
    state.chapter_17 = payload
  },
  UPDATE_chapter_18(state, payload) {
    state.chapter_18 = payload
  },
  UPDATE_chapter_19(state, payload) {
    state.chapter_19 = payload
  },

  //2x
  UPDATE_chapter_20(state, payload) {
    state.chapter_20 = payload
  },
  UPDATE_chapter_21(state, payload) {
    state.chapter_21 = payload
  },
  UPDATE_chapter_22(state, payload) {
    state.chapter_22 = payload
  },
  UPDATE_chapter_23(state, payload) {
    state.chapter_23 = payload
  },
  UPDATE_chapter_24(state, payload) {
    state.chapter_24 = payload
  },
  UPDATE_chapter_25(state, payload) {
    state.chapter_25 = payload
  },
  UPDATE_chapter_26(state, payload) {
    state.chapter_26 = payload
  },
  UPDATE_chapter_27(state, payload) {
    state.chapter_27 = payload
  },
  UPDATE_chapter_28(state, payload) {
    state.chapter_28 = payload
  },
  UPDATE_chapter_29(state, payload) {
    state.chapter_29 = payload
  },

  //3x
  UPDATE_chapter_30(state, payload) {
    state.chapter_30 = payload
  },
  UPDATE_chapter_31(state, payload) {
    state.chapter_31 = payload
  },
  UPDATE_chapter_32(state, payload) {
    state.chapter_32 = payload
  },
  UPDATE_chapter_33(state, payload) {
    state.chapter_33 = payload
  },
  UPDATE_chapter_34(state, payload) {
    state.chapter_34 = payload
  },
  UPDATE_chapter_35(state, payload) {
    state.chapter_35 = payload
  },
  UPDATE_chapter_36(state, payload) {
    state.chapter_36 = payload
  },
  UPDATE_chapter_37(state, payload) {
    state.chapter_37 = payload
  },
  UPDATE_chapter_38(state, payload) {
    state.chapter_38 = payload
  },
  UPDATE_chapter_39(state, payload) {
    state.chapter_39 = payload
  },

  //   4x
  UPDATE_chapter_40(state, payload) {
    state.chapter_40 = payload
  },
  UPDATE_chapter_41(state, payload) {
    state.chapter_41 = payload
  },
  UPDATE_chapter_42(state, payload) {
    state.chapter_42 = payload
  },
  UPDATE_chapter_43(state, payload) {
    state.chapter_43 = payload
  },
  UPDATE_chapter_44(state, payload) {
    state.chapter_44 = payload
  },
  UPDATE_chapter_45(state, payload) {
    state.chapter_45 = payload
  },
  UPDATE_chapter_46(state, payload) {
    state.chapter_46 = payload
  },
  UPDATE_chapter_47(state, payload) {
    state.chapter_47 = payload
  },
  UPDATE_chapter_48(state, payload) {
    state.chapter_48 = payload
  },
  UPDATE_chapter_49(state, payload) {
    state.chapter_49 = payload
  },

  //   5x
  UPDATE_chapter_50(state, payload) {
    state.chapter_50 = payload
  },
  UPDATE_chapter_51(state, payload) {
    state.chapter_51 = payload
  },
  UPDATE_chapter_52(state, payload) {
    state.chapter_52 = payload
  },
  UPDATE_chapter_53(state, payload) {
    state.chapter_53 = payload
  },
  UPDATE_chapter_54(state, payload) {
    state.chapter_54 = payload
  },
  UPDATE_chapter_55(state, payload) {
    state.chapter_55 = payload
  },
  UPDATE_chapter_56(state, payload) {
    state.chapter_56 = payload
  },
  UPDATE_chapter_57(state, payload) {
    state.chapter_57 = payload
  },
  UPDATE_chapter_58(state, payload) {
    state.chapter_58 = payload
  },
  UPDATE_chapter_59(state, payload) {
    state.chapter_59 = payload
  },

  // 6x
  UPDATE_chapter_60(state, payload) {
    state.chapter_60 = payload
  },
  UPDATE_chapter_61(state, payload) {
    state.chapter_61 = payload
  },
  UPDATE_chapter_62(state, payload) {
    state.chapter_62 = payload
  },
  UPDATE_chapter_63(state, payload) {
    state.chapter_63 = payload
  },
  UPDATE_chapter_64(state, payload) {
    state.chapter_64 = payload
  },
  UPDATE_chapter_65(state, payload) {
    state.chapter_65 = payload
  },
  UPDATE_chapter_66(state, payload) {
    state.chapter_66 = payload
  },
  UPDATE_chapter_67(state, payload) {
    state.chapter_67 = payload
  },
  UPDATE_chapter_68(state, payload) {
    state.chapter_68 = payload
  },
  UPDATE_chapter_69(state, payload) {
    state.chapter_69 = payload
  },

  //  7x
  UPDATE_chapter_70(state, payload) {
    state.chapter_70 = payload
  },
  UPDATE_chapter_71(state, payload) {
    state.chapter_71 = payload
  },
  UPDATE_chapter_72(state, payload) {
    state.chapter_72 = payload
  },
  UPDATE_chapter_73(state, payload) {
    state.chapter_73 = payload
  },
  UPDATE_chapter_74(state, payload) {
    state.chapter_74 = payload
  },
  UPDATE_chapter_75(state, payload) {
    state.chapter_75 = payload
  },
  UPDATE_chapter_76(state, payload) {
    state.chapter_76 = payload
  },
  UPDATE_chapter_77(state, payload) {
    state.chapter_77 = payload
  },
  UPDATE_chapter_78(state, payload) {
    state.chapter_78 = payload
  },
  UPDATE_chapter_79(state, payload) {
    state.chapter_79 = payload
  },

  //8x
  UPDATE_chapter_80(state, payload) {
    state.chapter_80 = payload
  },
  UPDATE_chapter_81(state, payload) {
    state.chapter_81 = payload
  },
  UPDATE_chapter_82(state, payload) {
    state.chapter_82 = payload
  },
  UPDATE_chapter_83(state, payload) {
    state.chapter_83 = payload
  },
  UPDATE_chapter_84(state, payload) {
    state.chapter_84 = payload
  },
  UPDATE_chapter_85(state, payload) {
    state.chapter_85 = payload
  },
  UPDATE_chapter_86(state, payload) {
    state.chapter_86 = payload
  },
  UPDATE_chapter_87(state, payload) {
    state.chapter_87 = payload
  },
  UPDATE_chapter_88(state, payload) {
    state.chapter_88 = payload
  },
  UPDATE_chapter_89(state, payload) {
    state.chapter_89 = payload
  },

  //9x
  UPDATE_chapter_90(state, payload) {
    state.chapter_90 = payload
  },
  UPDATE_chapter_91(state, payload) {
    state.chapter_91 = payload
  },
  UPDATE_chapter_92(state, payload) {
    state.chapter_92 = payload
  },
  UPDATE_chapter_93(state, payload) {
    state.chapter_93 = payload
  },
  UPDATE_chapter_94(state, payload) {
    state.chapter_94 = payload
  },
  UPDATE_chapter_95(state, payload) {
    state.chapter_95 = payload
  },
  UPDATE_chapter_96(state, payload) {
    state.chapter_96 = payload
  },
  UPDATE_chapter_97(state, payload) {
    state.chapter_97 = payload
  },
  UPDATE_chapter_98(state, payload) {
    state.chapter_98 = payload
  },
  UPDATE_chapter_99(state, payload) {
    state.chapter_99 = payload
  },

  //10x
  UPDATE_chapter_100(state, payload) {
    state.chapter_100 = payload
  },
  UPDATE_chapter_101(state, payload) {
    state.chapter_101 = payload
  },
  UPDATE_chapter_102(state, payload) {
    state.chapter_102 = payload
  },
  UPDATE_chapter_103(state, payload) {
    state.chapter_103 = payload
  },
  UPDATE_chapter_104(state, payload) {
    state.chapter_104 = payload
  },
  UPDATE_chapter_105(state, payload) {
    state.chapter_105 = payload
  },
  UPDATE_chapter_106(state, payload) {
    state.chapter_106 = payload
  },
  UPDATE_chapter_107(state, payload) {
    state.chapter_107 = payload
  },
  UPDATE_chapter_108(state, payload) {
    state.chapter_108 = payload
  },
  UPDATE_chapter_109(state, payload) {
    state.chapter_109 = payload
  },

  //110x
  UPDATE_chapter_110(state, payload) {
    state.chapter_110 = payload
  },
  UPDATE_chapter_111(state, payload) {
    state.chapter_111 = payload
  },
  UPDATE_chapter_112(state, payload) {
    state.chapter_112 = payload
  },
  UPDATE_chapter_113(state, payload) {
    state.chapter_113 = payload
  },
  UPDATE_chapter_114(state, payload) {
    state.chapter_114 = payload
  }
}
