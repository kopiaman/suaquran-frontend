// import { api } from '~/api'

export const state = () => ({
  // list of chapters
  chapters: [],

  //chapter info\
  chapter: '',

  // verses of chapter
  verses: [],
  // pagination meta
  page_meta: '',

  //use in bookmark to track what is the recent meta to load
  current_page_meta: ''
})

export const mutations = {
  UPDATE_chapters(state, payload) {
    state.chapters = payload
  },
  UPDATE_chapter(state, payload) {
    state.chapter = payload
  },

  UPDATE_verses(state, payload) {
    state.verses = payload
  },
  APPEND_verses(state, payload) {
    payload.forEach(item => {
      state.verses.push(item)
    })
  },

  UPDATE_page_meta(state, payload) {
    state.page_meta = payload
  },
  UPDATE_current_page_meta(state, payload) {
    state.current_page_meta = payload
  }
}

export const actions = {
  async FETCH_CHAPTERS(context) {
    const params = {
      language_name:
        context.rootState.setting.user_setting.verse_translation_language
    }
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get('/chapters', { params })

    context.commit('UPDATE_chapters', res)
  },

  async FETCH_CHAPTER(context, id) {
    const params = {
      language_name:
        context.rootState.setting.user_setting.verse_translation_language
    }
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(`/chapters/${id}`, { params })

    context.commit('UPDATE_chapter', res)

    context.commit(`chapter_info_bank/UPDATE_chapter_${id}`, res, {
      root: true
    })

    return res
  },

  async FETCH_VERSES(context, id) {
    const params = {
      // translations: 39,
      // language: 'id',
      language_name:
        context.rootState.setting.user_setting.verse_translation_language,
      limit: 10
    }
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(`/chapters/${id}/verses`, { params })

    // context.commit('UPDATE_verses', res.data)

    context.commit(`chapter_bank/UPDATE_chapter_${id}`, res.data, {
      root: true
    })

    let meta = actions.FETCH_META(res)

    context.commit('UPDATE_page_meta', meta)

    return res.data
  },

  async FETCH_VERSES_MORE(context, parameters) {
    const params = {
      // translations: 39,
      // language: 'id',
      page: parameters.page,
      language_name:
        context.rootState.setting.user_setting.verse_translation_language,
      limit: 10
    }

    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(
      `/chapters/${parameters.chapter_id}/verses`,
      {
        params
      }
    )

    // context.commit('APPEND_verses', res.data)

    context.commit(
      `chapter_bank/APPEND_chapter_${parameters.chapter_id}`,
      res.data,
      {
        root: true
      }
    )

    let meta = actions.FETCH_META(res)

    context.commit('UPDATE_page_meta', meta)

    return res.data
  },

  FETCH_META(res) {
    let current_page = res.current_page
    let next_page = null
    if (res.next_page_url) {
      next_page = current_page + 1
    }

    let prev_page = null
    if (res.prev_page_url) {
      prev_page = current_page - 1
    }

    let meta = {
      current_page: res.current_page,
      next_page: next_page,
      prev_page: prev_page,
      total_pages: res.last_page,
      total_count: res.total
    }

    return meta
  }
}
