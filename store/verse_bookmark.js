export const state = () => ({
  current_verse: '',
  verses: []
})

export const mutations = {
  UPDATE_verses(state, payload) {
    state.verses = payload
  },
  UPDATE_current_verse(state, payload) {
    state.current_verse = payload
  }
}

export const actions = {
  async STORE(context, data) {
    const form_data = {
      bookmark_id: data.bookmark_id,
      verse_id: data.verse_id
    }

    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$post(`/verse_bookmarks`, form_data)
    return res
  },

  async REMOVE(context, id) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$delete(`/verse_bookmarks/${id}/remove`)
    return res
  }
}
