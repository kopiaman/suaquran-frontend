export const state = () => ({
	// skipped_verses: [],
	// pagination meta
	page_meta: '',
	//use in bookmark to track what is the recent meta to load
	current_page_meta: ''

})

export const mutations = {
	// UPDATE_skipped_verses(state, payload) {
	// 	state.skipped_verses = payload
	// },
	// APPEND_skipped_verses(state, payload) {
	// 	payload.forEach(item => {
	// 		state.skipped_verses.push(item)
	// 	})
	// },
	UPDATE_page_meta(state, payload) {
		state.page_meta = payload
	},
	UPDATE_current_page_meta(state, payload) {
		state.current_page_meta = payload
	}
}

export const actions = {

	async FETCH_SKIPPED_VERSES(context, parameters) {

		const language_name = context.rootState.setting.user_setting.verse_translation_language

		this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
		const res = await this.$axios.$get(`/verses/skipped`, { params: { ...parameters, language_name } })


		context.commit(`verse_bank/UPDATE_chapter_${parameters.chapter_id}`, res.data, { root: true })

		let meta = actions.FETCH_META(res)

		context.commit('UPDATE_page_meta', meta)

		return res.data
	},

	async FETCH_SKIPPED_VERSES_MORE(context, parameters) {

		const language_name = context.rootState.setting.user_setting.verse_translation_language

		this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
		const res = await this.$axios.$get(`/verses/skipped`, { params: { ...parameters, language_name } })

		context.commit(`verse_bank/APPEND_chapter_${parameters.chapter_id}`, res.data, { root: true })

		let meta = actions.FETCH_META(res)

		context.commit('UPDATE_page_meta', meta)

		return res.data
	},


	async FETCH_SET_VERSES(context, parameters) {

		const language_name = context.rootState.setting.user_setting.verse_translation_language

		this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
		const res = await this.$axios.$get(`/verses/set`, { params: { ...parameters, language_name } })
		return res

	},



	FETCH_META(res) {
		let current_page = res.current_page
		let next_page = null
		if (res.next_page_url) {
			next_page = current_page + 1
		}

		let prev_page = null
		if (res.prev_page_url) {
			prev_page = current_page - 1
		}

		let meta = {
			current_page: res.current_page,
			next_page: next_page,
			prev_page: prev_page,
			total_pages: res.last_page,
			total_count: res.total
		}

		return meta
	}
}