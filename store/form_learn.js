export const state = () => ({
  //   participants: [],

  name: null,
  phone: null,
  email: null,

  street_address: null,
  zipcode: null,
  city: null,
  state: '',
  country: 'Malaysia',

  participants: null,

  method: '',
  topic: '',

  commitment: '',
  day_available: [],
  time_available: []
})

export const mutations = {
  // personal
  UPDATE_name(state, payload) {
    state.name = payload
  },
  UPDATE_phone(state, payload) {
    state.phone = payload
  },
  UPDATE_email(state, payload) {
    state.email = payload
  },

  //address
  UPDATE_street_address(state, payload) {
    state.street_address = payload
  },

  UPDATE_zipcode(state, payload) {
    state.zipcode = payload
  },

  UPDATE_city(state, payload) {
    state.city = payload
  },
  UPDATE_state(state, payload) {
    state.state = payload
  },
  UPDATE_country(state, payload) {
    state.country = payload
  },

  //participants
  UPDATE_participants(state, payload) {
    state.participants = payload
  },

  //modules
  UPDATE_method(state, payload) {
    state.method = payload
  },
  UPDATE_topic(state, payload) {
    state.topic = payload
  },

  //schedule
  UPDATE_commitment(state, payload) {
    state.commitment = payload
  },

  UPDATE_day_available(state, payload) {
    state.day_available = payload
  },

  UPDATE_time_available(state, payload) {
    state.time_available = payload
  },

  CLEAR_STATE(state) {
    state.name = null
    state.phone = null
    state.email = null
    // state.method = ''
    // state.topic = ''
    // state.commitment = ''
    // state.day_available = []
    // state.time_available = []
  }
}

export const actions = {
  async FORM_SUBMIT(context, params) {
    const res = await this.$axios.$post(`/form_learn`, { ...params })
    return res
  }
}
