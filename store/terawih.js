export const state = () => ({
  applicants: [],
  stats: ''
})

export const mutations = {
  UPDATE_applicants(state, payload) {
	state.applicants = payload
  },
  UPDATE_stats(state, payload) {
		  state.stats = payload
	  },
}

export const actions = {
  async STORE(context, params) {
	
	const options = {
	  // headers: { Authorization: `Bearer ${context.rootState.auth.api_token}` }
	}
	const res = await this.$axios.$post(`/applicants`, {...params} )

	return res

	// context.commit('UPDATE_verses', res.verses)
  },

  async STATS(context) {
	
	const res = await this.$axios.$get(`/applicants/stats_for_today`, {} )

	context.commit('UPDATE_stats', res)

	return
  },

  async APPLICANTS_FOR_TODAY(context, gender) {
	
	const res = await this.$axios.$get(`/applicants/applicant_for_today/${gender}`, {} )

	context.commit('UPDATE_applicants', res)

	return
  }
}