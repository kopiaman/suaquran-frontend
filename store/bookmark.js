export const state = () => ({
  bookmarks: [],
  bookmarks_with_verses: []
})

export const mutations = {
  UPDATE_bookmarks(state, payload) {
    state.bookmarks = payload
  },
  UPDATE_bookmarks_with_verses(state, payload) {
    state.bookmarks_with_verses = payload
  }
}

export const actions = {
  async INDEX(context) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(`/bookmarks`)

    context.commit('UPDATE_bookmarks', res)
  },

  async INDEX_WITH_VERSES(context) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$get(`/bookmarks/with_verses`)

    context.commit('UPDATE_bookmarks_with_verses', res)
  },

  async STORE(context, data) {
    const form_data = {
      name: data.name,
      color: 'white'
    }
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$post(`/bookmarks`, form_data)
    return res
  },

  async UPDATE(context, data) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    let param = {
      name: data.name,
      id: data.id,
      color: 'white'
    }
    const res = await this.$axios.$put(`/bookmarks/${param.id}/update`, param)
  },

  async REMOVE(context, id) {
    this.$axios.setToken(context.rootState.auth.jwtToken, 'Bearer')
    const res = await this.$axios.$delete(`/bookmarks/${id}/remove`)
    return res
  }
}
