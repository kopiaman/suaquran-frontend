export const state = () => ({
  isAuthenticated: false,
  jwtToken: [],
  name: null,
  email: null,
  user: null,
})

export const mutations = {
  UPDATE_isAuthenticated(state, payload) {
    state.isAuthenticated = payload
  },
  UPDATE_jwtToken(state, payload) {
    state.jwtToken = payload
  },
  UPDATE_name(state, payload) {
    state.name = payload
  },
  UPDATE_email(state, payload) {
    state.email = payload
  },
  UPDATE_user(state, payload) {
    state.user = payload
  }
}

export const actions = {
  async ME(context, token) {
    this.$axios.setToken(token, 'Bearer')
    const res = await this.$axios.$get('/auth/me')

    if (res) {
      context.commit('UPDATE_jwtToken', token)
      context.commit('UPDATE_name', res.name)
      context.commit('UPDATE_email', res.email)
      context.commit('UPDATE_user', res)
      context.commit('UPDATE_isAuthenticated', true)
    }

    //load user setting
    context.dispatch('setting/FETCH', {}, { root: true })

    return res
  },

  async LOGOUT(context) {
    context.commit('UPDATE_jwtToken', '')
    context.commit('UPDATE_name', '')
    context.commit('UPDATE_email', '')
    context.commit('UPDATE_isAuthenticated', false)

    return {"message" : "Signout Successful"}
  },

  async LOGIN(context, params) {
   
    const body = { email:params.email, password: params.password }
    const res = await this.$axios.$post('/auth/login', body)
    

    if (res) {
      await context.dispatch('ME', res.access_token)
    }

    return res
  },

  async REGISTER(context, params) {
   
    const body = { email:params.email, password: params.password , name:params.name}
    const res = await this.$axios.$post('/auth/register', body)
    
    return res
  },


  async VERIFY(context, code) {
   
    const res = await this.$axios.$get(`/auth/verify/${code}`)
    
    return res
  },

  async CHANGE_PASSWORD(context, params) {
   
    const body = { password:params.password, confirm_password: params.confirm_password}
    const res = await this.$axios.$post('/auth/change_password', body)
    
    return res
  },

  async RESET_PASSWORD(context, params) {
   
    const body = { email:params.email}
    const res = await this.$axios.$post('/auth/reset_password', body)
    
    return res
  },

  async CONFIRM_RESET_PASSWORD(context, params) {
   
    const body = { email:params.email, id: params.id}
    const res = await this.$axios.$post('/auth/confirm_reset_password', body)
    
    return res
  },


  async DELETE_ACCOUNT(context) {
   
    const res = await this.$axios.$post('/auth/delete')
    
    return res
  },






}
