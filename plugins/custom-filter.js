import Vue from 'vue'

//example pad(5,3) return 005
Vue.filter('pad', function(num, size) {
  var s = num + ''
  while (s.length < size) s = '0' + s
  return s
})

//example chapter_verse_code(1,15,3) return 001015
Vue.filter('chapter_verse_code', function(chapter, verse, size) {
  var chapters = chapter + ''
  while (chapters.length < size) chapters = '0' + chapters

  var verses = verse + ''
  while (verses.length < size) verses = '0' + verses

  return chapters + verses
})
