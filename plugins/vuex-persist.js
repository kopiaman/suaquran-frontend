// import createPersistedState from 'vuex-persistedstate'

// export default ({ store }) => {
//   window.onNuxtReady(() => {
//     createPersistedState({
//       key: 'vuex_suaquran',
//       storage: window.localforage
//     })(store)
//   })
// }
import localForage from 'localforage'
import VuexPersistence from 'vuex-persist'

export default ({ store }) => {
  window.onNuxtReady(() => {
    new VuexPersistence({
      /* your options */
      key: 'vuex_suaquran',
      storage: localForage
    }).plugin(store)
  })
}
