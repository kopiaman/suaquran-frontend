import { FALSE } from 'node-sass'

require('dotenv').config()
export default {
  mode: 'universal',
  debug: false,

  pwa: {
    icon: {
      purpose: ['icon', 'maskable']
    },
    manifest: {
      name: 'SuaQuran',
      background_color: '#8309e6'
    },
    meta: {
      theme_color: '#ffffff',
      name: 'SuaQuran.com | Aplikasi Quran Semua Dalam Satu',
      viewport: 'width=device-width, initial-scale=1, user-scalable=no',
      // contentType: 'text/html; charset=utf-8',
      author: 'Salman Kopiaman',
      ogTitle: 'SuaQuran.com | Aplikasi Quran Semua Dalam Satu',
      description:
        'Aplikasi Quran untuk baca,hafaz dan mengaji secara online di Malaysia',
      ogDescription:
        'Aplikasi Quran untuk baca,hafaz dan mengaji secara online di Malaysia',
      ogHost: 'https://suaquran.com',
      ogImage:
        'https://suaquran.fra1.digitaloceanspaces.com/suaquran_meta.jpg'
    }
  },
  

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#06ccd4', height: '10px' },
  /*
   ** Global CSS
   */
  css: ['~/css/main.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/axios'},
    { src: '~/plugins/vuex-persist', mode: 'client' },
    { src: '~/plugins/vuex-cache.js', mode: 'client' },
    { src: '~/plugins/vue2-filters', mode: 'client' },
    { src: '~/plugins/sweet-modal', mode: 'client' },
    { src: '~/plugins/sweet-alert2', mode: 'client' },
    { src: '~/plugins/custom-filter', mode: 'client' },
    { src: '~/plugins/vue-slider', mode: 'client' },
    { src: '~/plugins/vue-progressive-image', mode: 'client' }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/localforage',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-164934154-1'
      }
    ],
   
    
    
  ],

  localforage: {
    name: 'localforage',
    storeName: 'keyvaluepairs'
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/pwa',
    '@nuxtjs/onesignal',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/toast',
    ['@nuxtjs/google-tag-manager', { id: 'GTM-5ND6LPR', layer: "dataLayer" }],
    ['nuxt-facebook-pixel-module', {
      /* module options */
      track: 'PageView',
      pixelId: '345975823310181',
      autoPageView: true,
      disabled: false
    }],
    '@nuxtjs/sentry'
    
  ],

  // log reporting
  sentry: {
    dsn: process.env.SENTRY_DSN, // Enter your project's DSN here
    config: {}, // Additional config
  },

  // Options
  oneSignal: {
    init: {
      appId: process.env.ONE_SIGNAL_ID,
      allowLocalhostAsSecureOrigin: true,
      welcomeNotification: {
        disable: true
      }
    }
  },

  toast: {
    position: 'bottom-right',
    duration: 1000
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BACKEND_URL + '/api',
    rejectUnauthorized: false,
    crossdomain: true
  },

  proxy: {
    '/api': {
      target: 'https://verses.quran.com',
      pathRewrite: {
        '^/api': '/'
      }
    }
  },

  /*
   ** Build configuration
   */
  build: {
    // analyze: true,
    transpile: ['sweet-modal', 'vuex-cache', 'vuex-persist'],

    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      //fix problem dependancy fs
      config.node = {
        fs: 'empty'
      }
    }
  },

  purgeCSS: {
    // your settings here
    enabled: false
  },

  workbox: {
    // offlineStrategy: 'StaleWhileRevalidate',
    runtimeCaching: [
      {
        //image resizeer
        urlPattern: 'https://auudwpoeen.cloudimg.io/.*',
        handler: 'cacheFirst'
      },
      {
        // Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
        urlPattern: 'https://omj.sgp1.digitaloceanspaces.com/suaquran/fonts/.*',
        // Defaults to `networkFirst` if omitted
        handler: 'cacheFirst'
        // Defaults to `GET` if omitted
        // method: 'GET'
      },
      {
        // Should be a regex string. Compiles into new RegExp('https://my-cdn.com/.*')
        urlPattern: 'https://backend.suaquran.com/api/chapters/.*',
        // Defaults to `networkFirst` if omitted
        handler: 'staleWhileRevalidate',
        strategyOptions: {
          cacheName: 'chapter-cache',
          cacheExpiration: {
            //seconds x minutes x hour x days
            maxAgeSeconds: 60 * 60 * 24 * 30
          }
        }
      },
      {
        urlPattern: 'https://backend.suaquran.com/api/auth/.*',
        handler: 'staleWhileRevalidate',
        method: 'GET, POST',
        strategyOptions: {
          cacheName: 'auth-cache',
          cacheExpiration: {
            //seconds x minutes x hour x days
            maxAgeSeconds: 60 * 60 * 24 * 30
          }
        }
      }
    ]
  },

  /*
   ** Headers of the page
   */
  head: {
    title: 'Mengaji Quran Online | SuaQuran.com - Aplikasi Quran Semua Dalam Satu ',
    description: `Aplikasi Quran untuk baca,hafaz dan mengaji secara online di Malaysia `,
    htmlAttrs: {
			lang: 'ms-MY'
		},
    meta: [
      { charset: 'utf-8' },
      { 'http-equiv': 'Content-Type', content: 'text/html; charset=utf-8' },
      { 'http-equiv': 'Content-Language', content: 'EN;AR' },
      { name: 'Charset', content: 'UTF-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'keywords',
        name: 'keywords',
        content: `quran online malaysia, cari guru quran, belajar quran, al-quran, koran, belajar Quran Online, Talaqqi Malaysia, Cikgu Quran KL Selangor`
      },
      {name:"apple-mobile-web-app-capable", content:"yes"},
      { name: "mobile-web-app-capable" , content:"yes"},
      // {
      // 	name: 'google-site-verification',
      // 	content: 'qIkGWH5cLLFi1afYTRdrxxWghVqNltusbNrKk0ui0rY'
      // },
    ],
    link: [
      {
        rel: "apple-touch-icon",
        sizes: "57x57",
        href: "/favicons/apple-icon-57x57.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "60x60",
        href: "/favicons/apple-icon-60x60.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "72x72",
        href: "/favicons/apple-icon-72x72.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "76x76",
        href: "/favicons/apple-icon-76x76.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "114x114",
        href: "/favicons/apple-icon-114x114.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "120x120",
        href: "/favicons/apple-icon-120x120.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "144x144",
        href: "/favicons/apple-icon-144x144.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "152x152",
        href: "/favicons/apple-icon-152x152.png"
      },
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/favicons/apple-icon-180x180.png"
      },

      {
        rel: "icon",
        type: "image/png",
        sizes: "192x192",
        href: "/favicons/android-icon-192x192.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "32x32",
        href: "/favicons/favicons-32x32.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "96x96",
        href: "/favicons/favicons-96x96.png"
      },
      {
        rel: "icon",
        type: "image/png",
        sizes: "16x16",
        href: "/favicons/favicons-16x16.png"
      },

    
      // splash for apple
      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/iphone5_splash.png',
        media:
          '(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },
      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/iphone6_splash.png',
        media:
          '(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },
      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/iphonex_splash.png',
        media:
          '(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)'
      },
     
      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/iphonexr_splash.png',
        media:
          '(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },

      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/iphonexsmax_splash.png',
        media:
          '(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)'
      },

      // tablet splash
      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/ipad_splash.png',
        media:
          '(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },

      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/ipadpro1_splash.png',
        media:
          '(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },
      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/ipadpro3_splash.png',
        media:
          '(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },

      {
        rel: 'apple-touch-startup-image',
        href: '/splashscreens/ipadpro2_splash.png',
        media:
          '(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)'
      },  


    ]
  },

  // env: {
  //   baseUrl: process.env.BASE_URL || 'http://localhost:3000',
  //   backendUrl: process.env.BACKEND_URL || 'https://suaquran-backend.test'
  // }
}

