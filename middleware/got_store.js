export default function({ store, redirect }) {
  if (!store.auth && process.server) {
    console.log('ssr no authenticated')
    return redirect('/')
  }
}
